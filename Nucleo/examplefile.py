import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
#tim2 = pyb.Timer(2, freq = 20000)
#t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
#t2ch1.pulse_width_percent(100)

## -- ENABLE (Set PA15 HI) -- ##
pinA15 = pyb.Pin (pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pinA15.high ()
## - Set IN1A (PB4) LO -- ##
pinB1 = pyb.Pin (pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
pinB1.low ()

## -- Set IN2A (PB5) PWM -- ##
pinB0 = pyb.Pin (pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
tim3 = pyb.Timer(3, freq = 20000)                    
t3ch1 = tim3.channel(3, pyb.Timer.PWM, pin=pinB0)
t3ch1.pulse_width_percent(1)
