''' @file btconnect.py - This file is used to calculate a sequence of fibonacci 
numbers

E. Wade, 08.26.2020'''
import pyb
from pyb import UART

# Create variable to store BT data

# Provide baud rate 9600 to BT module on Serial 3
uart = UART(3, 9600)
uart.init(9600, bits=8, parity=None, stop=1) # init with given parameters

# Set pin as output
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

# Read from BT

''' Write a section of code that always checks for the buffer. When something
is sent, it then prints the sent value '''
def btc():
    while True:
        if uart.any() != 0:
            curval = uart.readline()
            curval2 = int(curval)
            if curval2 == 0:
                print(curval2,' turns it OFF')
                pinA5.low()
                uart.write('')
                uart.write('You turned it off. MEH.')
            else:
                print(curval2,' turns it ON')
                pinA5.high()
                uart.write('')
                uart.write('You turned it on. WOW.')
        else:
            pass

#if __name__ == '__main__':
    #btc()