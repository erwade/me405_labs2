## @file BTmodule.py
#  This file contains the Bluetooth (BLE) module.
#  @package BTmodulei

import pyb
from pyb import UART

class BTconn:

    def __init__(self,unumb, ubaud, ubits, upari, ustop):
        ## The pin used for channel A
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        # Specify UART
        self.unumb = unumb
        # Specify Baud rate
        self.ubaud = ubaud
        # Specify number of bits
        self.ubits = ubits
        # Specify parity bits
        self.upari = upari
        # Specify stop bits
        self.ustop = ustop
        
        # Set A5 as output pin
        
        self.uart = UART(unumb)
        self.uart.init(ubaud, bits=ubits, parity=upari, stop=ustop)
    
    def any(self):
        vals = self.uart.any()
        return vals
            
    def write(self, val):
        self.val = val
        self.uart.write(str(val))
        
    def readline(self):
        vals = self.uart.readline()
        return vals
        
    def on(self):
        self.pinA5.high()
        
    def off(self):
        self.pinA5.low()

