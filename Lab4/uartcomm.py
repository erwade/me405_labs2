''' @file btconnect.py - This file is used to calculate a sequence of fibonacci 
numbers

E. Wade, 08.26.2020'''
import pyb
from pyb import UART
import utime

# Create variable to store BT data
myuart = UART(2)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

for n in range (5):
    if myuart.any() != 0:
        curval = myuart.readline()
        curval2 = int(curval)
        myuart.write('value is: ' + curval2)
    myuart.write('Waiting: ' + str(utime.ticks_us()) + '\r\n')
    pinA5.high()
    utime.sleep_ms(1000)

    pinA5.low()
    utime.sleep_ms(1000)
    
# Provide baud rate 9600 to BT module on Serial 3

