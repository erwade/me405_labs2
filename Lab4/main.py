import pyb
import utime
from pyb import UART
from encoder2 import QEncoder

myenco = QEncoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, pyb.Timer(4),1000)
myuart = UART(2)

while True:
    myenco.update()
    if myuart.any() != 0:
        val = myuart.readchar()
        if val == 71:
            myuart.write('{:},{:}'.format(utime.ticks_us(),myenco.get_position()) + '\n\r')
        else:
            pass