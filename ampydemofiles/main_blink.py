# -*- coding: utf-8 -*-
"""
@file main_blink.py

@author: wadee
"""
#from FSM_blinky_inc import TaskBlinker
from FSM_blink import TaskBlink

#task1 = TaskBlinker(10, 1)
task1 = TaskBlink(3, 1) #Set period and LED type (0 = virtual / 1 = physical)
task2 = TaskBlink(1, 0)
for n in range(100000):
    task1.run()
    task2.run()




