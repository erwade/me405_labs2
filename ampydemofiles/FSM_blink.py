## @file FSM_blink
# @brief
# @details
# @author
# @date
import pyb
import time

class TaskBlink:
    '''
    '''
    
    ## Constants for states 0-4
    S0_INIT   = 0
    S1_LEDON  = 1
    S2_LEDOF  = 2

    def __init__(self, interval, bmode):
        self.state = self.S0_INIT
        self.interval = interval
        self.bmode = bmode
        self.runs = 0
        
        ## Set up timing variables
        self.start_time = time.time()        
        self.interval = interval           
        self.next_time = self.start_time + self.interval        

    def run(self):
        pinA5=pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        self.curr_time = time.time()
        #pyb.delay(500)
        if (self.curr_time >= self.next_time):
            if (self.state == self.S0_INIT):
                print(str(self.runs) + ' Initialization')
                self.transitionTo(self.S1_LEDON)
    
            elif (self.state == self.S1_LEDON):
                #print(str(self.runs))
                if self.bmode == 1:
                    pinA5.high()
                else:
                    print('Virtual LED ON')
                self.transitionTo(self.S2_LEDOF)
            elif (self.state == self.S2_LEDOF):
                #print(str(self.runs))
                if self.bmode == 1:
                    pinA5.low()
                else:
                    print('Virtual LED OFF')
                self.transitionTo(self.S1_LEDON)
                
            else:
                pass
            
            self.runs += 1
            self.next_time += self.interval
        
    def transitionTo(self, newState):
        self.state = newState


