''' 
@file       lab1.py
@brief      Calculates the Fibonacci number at any given index of the Fibonacci sequence from a user input.
@author     Dakota Baker
@date       September 28, 2020 
'''

''' 
@brief      This variable holds the user's desired index number
@details    Description
'''
idx = 0;
''' 
@brief      This variable holds the user's desired index number
@details    Description
'''
class Fibonnaci:
    '''
    @brief      Fibonacci Sequence Calculator
    @details    Calculates the Fibonacci number at any given index of the Fibonacci sequence from a user input.
    '''
    
    def fib(idx):
        '''
        @brief      Function which calculates the Fibonacci number
        @details    This function asks for a user desired index of the Fibonacci sequence (0, 1, 1, 2, 3, 5, 8, ...). This input is stored in the variable @ref idx. 
                    A bottom-up approach using a list of two variables [x0, x1] plus a temporary variable (which sums x0 and x1) is then enacted to calculate the number at the inputed index.
        '''
        print('-----------------------------------------------------------------')
        print("[Type 'end' to quit]")
        while True:
            ''' 
            @brief      This variable holds the user's desired index number
            @details    Description
            '''
            idx = 0;
            idx = input("Enter an index of the Fibonacci squence: ")
            ## Some text
            #
            # Some more text
            if idx == 'end':
                break
            else:
                try:
                    idx = int(idx)
                    if idx == 0:
                        print(0)
                    elif idx == 1 or idx == 2:
                        print(1)
                    elif idx > 2:
                        [x0, x1] = [0, 1]
                        i = 2
                        while i <= idx:
                            temp = x0 + x1
                            x0 = x1
                            x1 = temp
                            i = i + 1
                        print(x1)
                    else:
                        print('Only positive numbers are accepted')
                except ValueError:
                    print('Only numbers are accepted');
    
    if __name__ == '__main__':
        fib(0)
