'''
@file mainpage.py

@mainpage

@section sec_lab1 Lab 1: Fibonacci Sequence
The goal for this lab was to create a python script that could calculate the number at a given index in the Fibonacci sequence.

Documentation can be found here: @ref lab1.py

The file itself can be found here: https://bitbucket.org/MustardSeed55/me305/src/master/lab1.py
  
@author Dakota Baker 

@date September 26, 2020
'''