"""
@file FSM_example.py

Example of a fsm

@author wadee
"""


import utime
import pyb
from pyb import UART

class TaskUI:
    '''
    @ brief         Windshield wiper FSM
    @details        This class implements a FSM that mimics a windshield wiper
    '''
    
    ## Constant for State 0 (init)
    S0_INIT             = 0
    S1_RUNINT           = 1
    
    def __init__(self, interval):
        '''
        '''
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        self.runs = 0
        
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## Interval of time (in s) between task runs
        self.interval = int(interval*1e6)

        ## Timestamp for when to run task next            
        self.next_time = utime.ticks_add(self.start_time, int(interval))
        
        ## UART object
        self.myuart = UART(2)
    
    def run(self):
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_RUNINT)
                print('''
                      [ Press 'z' ] ==> zero encoder
                      [ Press 'd' ] ==> obtain delta
                      [ Press 'p' ] ==> print
                ''')

            elif(self.state == self.S1_RUNINT):

                if self.myuart.any():
                    print('Key pressed: ' + str(self.myuart.readchar()))
                self.transitionTo(self.S1_RUNINT)
                
            else:
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):            
        self.state = newState



# Could be in another file
    
task1 = TaskUI(0.1) 

while True:
    task1.run()

