# -*- coding: utf-8 -*-
"""
@file main.py

Created on Sun Oct  4 14:04:51 2020

@author: wadee
"""
from FSM_example import Button, MotorDriver, TaskWind
from FSM_blink import TaskBlink
# Could be in another file
    
Motor1 = MotorDriver()

GoButton1 = Button('PB6')
LeftLimit1 = Button('PB7')
RightLimit1 = Button('PB8')

task1 = TaskWind(0.5, Motor1, GoButton1, LeftLimit1, RightLimit1)
task2 = TaskWind(0.5, Motor1, GoButton1, LeftLimit1, RightLimit1)
task3 = TaskBlink(1,3)

for N in range(20000000):
    task1.run()
    task2.run()
    task3.run()



