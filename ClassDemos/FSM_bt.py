"""
@file FSM_example.py

Example of a fsm

@author wadee
"""

import utime
from pyb import UART
from BTmodule import BTconn

class BTreader:
    '''
    @ brief         Windshield wiper FSM
    @details        This class implements a FSM that mimics a windshield wiper
    '''
    
    ## Constant for State 0 (init)
    S0_INIT             = 0
    S1_RXMD             = 1
    S2_TXMD             = 2

    def __init__(self, interval, btmod):
        '''
        '''
        
        ## Initial State
        self.state = self.S0_INIT
        self.runs = 0
        self.start_time = utime.ticks_us()
 
        
        ## Interval of time (in s) between task runs
        self.interval = int(interval*1e6)
        self.btmod = btmod
        
        ## Timestamp for when to run task next            
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_RXMD)
                
            elif(self.state == self.S1_RXMD):
                print(str(self.runs) + ' State 1 ' + str(self.curr_time-self.start_time))
                if self.btmod.any() != 0:
                    self.transitionTo(self.S2_TXMD)
                
            elif(self.state == self.S2_TXMD):
                print(str(self.runs) + ' State 2 ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_RXMD)
                curval = self.btmod.readline()
                curval2 = int(curval)
                if curval2 == 0:
                    print(curval2,' turns it OFF')
                    self.btmod.off()
                    self.btmod.write('You turned it off. MEH.')
                else:
                    print(curval2,' turns it ON')
                    self.btmod.on()
                    self.btmod.write('You turned it on. YEH')
                
            else:
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):
            
        self.state = newState


# Equivalent Main File
    
mybt = BTconn(3,9600,8,None,1)

task1 = BTreader(0.1, mybt)


for N in range(200000000):
    task1.run()

