"""
@file FSM_elevator.py

Created on Sun Oct  4 16:28:03 2020

@author: wadee
"""
from random import choice

class TaskElevator:
    '''
    @brief        Elevator FSM
    @details      This class implements a 2-floor elevator.
    '''
    
    ## Constants for states 0-4
    S0_INITT   = 0
    S1_MOVDN   = 1
    S2_MOVUP   = 2
    S3_STOP1   = 3
    S4_STOP2   = 4
    
    def __init__(self, Motor, button_1, button_2, first, second, inst):
        '''
        '''
        ## State to run on the first task iteration
        self.state = self.S0_INITT
        
        ## Class attribute 'copy' of the motor, button, and floor objects
        self.button_1 = button_1
        self.button_2 = button_2
        self.first = first
        self.second = second
        self.inst = inst
        self.Motor = Motor
        
        self.runs = 0
        
    def run(self):
        if (self.state == self.S0_INITT):
            print(str(self.runs) + ' State 0')
            self.transitionTo(self.S1_MOVDN)
            self.Motor.Down()
            self.button_1.getButtonState(1)
            self.button_2.getButtonState(1)

        elif (self.state == self.S1_MOVDN):
            print(str(self.runs) + ' State 1')
            if self.first.getButtonState(0):
                self.Motor.Stop()
                self.transitionTo(self.S3_STOP1)
                self.button_1.getButtonState(1)
        elif (self.state == self.S2_MOVUP):
            print(str(self.runs) + ' State 2')
            if self.second.getButtonState(0):
                self.Motor.Stop()
                self.transitionTo(self.S4_STOP2)  
                self.button_2.getButtonState(1) 
        elif (self.state == self.S3_STOP1):
            print(str(self.runs) + ' State 3')
            if self.button_2.getButtonState(0):
                self.Motor.Up()
                self.transitionTo(self.S2_MOVUP)
            elif self.button_1.getButtonState(0):
                self.transitionTo(self.S3_STOP1)
        elif (self.state == self.S4_STOP2):
            print(str(self.runs) + ' State 4 ' + str(self.inst))
            if self.button_1.getButtonState(0):
                self.Motor.Down()
                self.transitionTo(self.S1_MOVDN)
            elif self.button_2.getButtonState(0):
                self.transitionTo(self.S4_STOP2)
        self.runs += 1
    
    def transitionTo(self, newState):
        self.state = newState
        
class Button:
    '''
    '''
    def __init__(self,pin):
        self.pin = pin
        print('Button object created attached to pin'+str(self.pin))
            
    def getButtonState(self, val):
        '''
        '''
        if val == 0:
            return choice ([True, False])
        else:
            return False

class MotorDriver:
    '''
    '''
    def __init__(self):
        pass
    def Down(self):
        print('Motor pulling DOWN')
            
    def Up(self):
        print('Motor pushing UP')
            
    def Stop(self):
        print('Motor stopped')
        
        
        
Motor = MotorDriver()

button_1 = Button('PB6')
button_2 = Button('PB7')
first = Button('PB8')
second = Button('PB9')

task1 = TaskElevator(Motor, button_1, button_2, first, second, 1)
task2 = TaskElevator(Motor, button_1, button_2, first, second, 2)

for N in range(1000):
    task1.run()
#   task2.run()
#    task3.run()
            
   