"""
@file dogclasses.py 

Created on Sun Oct  4 11:44:53 2020

@author E. Espinoza-Wade
"""


from random import randint

class Dog:
    '''
    @brief      This class represents a doggie
    @details    This doggie can (1) eat food and (2) bark
    '''
    
    def __init__(self):
        '''
        '''
        self.hungry = True
        self.growl()
    
    def eat(self, food):
        '''
        @brief
        @details
        '''
        self.hungry = False
        self.bark()
        print ('Excellent ' + food)
        
    def bark(self):
        '''
        @brief
        @return
        '''
        print (' Bark' + randint(0,9)*'Bark' + '!')
        
    def growl(self):
        '''
        '''
        print ('grrrrrrrrr')
