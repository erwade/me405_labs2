"""
@file FSM_example.py

Example of a fsm

@author wadee
"""


from random import choice
import time
#import utime

class TaskWind:
    '''
    @ brief         Windshield wiper FSM
    @details        This class implements a FSM that mimics a windshield wiper
    '''
    
    ## Constant for State 0 (init)
    S0_INIT             = 0
    S1_STOPPED_AT_L     = 1
    S2_STOPPED_AT_R     = 2
    S3_MOVING_L         = 3
    S4_MOVING_R         = 4
    S5_DONOTHING        = 5
    
    def __init__(self, interval, Motor, GoButton, LeftLimit, RightLimit):
        '''
        '''
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## Class attribute 'copy' of the motor object
        self.Motor = Motor
        
        ## Class attribute 'copy' of the GoButton object
        self.GoButton = GoButton
        self.LeftLimit = LeftLimit
        self.RightLimit = RightLimit
        
        self.runs = 0
        
        self.start_time = time.time()
        #self.start_time = utime.ticks_us()
        
        ## Interval of time (in s) between task runs
        self.interval = interval
        #self.interval = int(interval*1e6)

        ## Timestamp for when to run task next            
        self.next_time = self.start_time + self.interval
        #self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        self.curr_time = time.time()
        #self.curr_time = utime.ticks_us()
        if (self.curr_time >= self.next_time): 
        #if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S3_MOVING_L)
                self.Motor.Forward()
            elif(self.state == self.S1_STOPPED_AT_L):
                print(str(self.runs) + ' State 1 ' + str(self.curr_time-self.start_time))
                if self.runs*self.interval > 20:
                    self.transitionTo(self.S5_DONOTHING)
                elif self.GoButton.getButtonState():
                    self.transitionTo(self.S4_MOVING_R)
                    self.Motor.Reverse()
                
            elif(self.state == self.S2_STOPPED_AT_R):
                print(str(self.runs) + ' State 2 ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S3_MOVING_L)
                self.Motor.Forward()
                
            elif(self.state == self.S3_MOVING_L):
                print(str(self.runs) + ' State 3 ' + str(self.curr_time-self.start_time))
                if self.LeftLimit.getButtonState():
                    self.transitionTo(self.S1_STOPPED_AT_L)
                    self.Motor.Stopped()
                
            elif(self.state == self.S4_MOVING_R):
                print(str(self.runs) + ' State 4 ' + str(self.curr_time-self.start_time))
                if self.RightLimit.getButtonState():
                    self.transitionTo(self.S2_STOPPED_AT_R)
                    self.Motor.Stopped()
            elif(self.state == self.S5_DONOTHING):
                print('Doing nothing')
                
            else:
                pass
            
            self.runs += 1
            self.next_time += self.interval
            #self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):
            
        self.state = newState

class Button:
    '''
    '''
    def __init__(self,pin):
        self.pin = pin
        print('Button object created attached to pin'+str(self.pin))
        
    def getButtonState(self):
        '''
        '''
        return choice([True, False])
    
class MotorDriver:
    '''
    '''
    def __init__(self):
        '''
        '''
        pass
    
    def Forward(self):
        '''
        '''
        print('Motor moving CCW')
        
    def Reverse(self):
        '''
        '''
        print('Motor moving CW')

    def Stopped(self):
        '''
        '''
        print('Motor stopped')

# Could be in another file
    
Motor1 = MotorDriver()

GoButton1 = Button('PB6')
LeftLimit1 = Button('PB7')
RightLimit1 = Button('PB8')

task1 = TaskWind(0.5, Motor1, GoButton1, LeftLimit1, RightLimit1)
task2 = TaskWind(1, Motor1, GoButton1, LeftLimit1, RightLimit1)

for N in range(200000000):
    task1.run()
#    task2.run()
#    task3.run()
