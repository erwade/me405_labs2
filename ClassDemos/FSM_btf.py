"""
@file FSM_btf.py

Example of a fsm - blinks at a frequency set by the user

@author wadee
"""

import utime
from pyb import UART
from BTmodule import BTconn

class BTreader:
    '''
    @ brief         Windshield wiper FSM
    @details        This class implements a FSM that mimics a windshield wiper
    '''
    
    ## Constant for State 0 (init)
    S0_INIT             = 0
    S1_RXMD             = 1
    S2_TXMD             = 2

    def __init__(self, interval, cnts, btmod):
        '''
        '''
        
        ## Initial State
        self.state = self.S0_INIT
        self.runs = 0
        self.ints = 0
        self.indi = True
        self.start_time = utime.ticks_us()
        self.curval = 0
        
        ## Interval of time (in s) between task runs
        self.interval = int(interval*1e6)
        self.cnts = cnts
        self.btmod = btmod
        
        ## Timestamp for when to run task next            
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_RXMD)
                
            elif(self.state == self.S1_RXMD):
                print(str(self.runs) + ' State 1 ' + str(self.curr_time-self.start_time))
                if self.btmod.any() != 0:
                    self.curval = int(self.btmod.readline())
                    self.curval2 = self.curval
                    self.transitionTo(self.S2_TXMD)
                
            elif(self.state == self.S2_TXMD):
                print(str(self.runs) + ' State 2, curval = ' + str(self.curval))
                if self.btmod.any() != 0:
                    self.transitionTo(self.S1_RXMD)
                else:
                    if (utime.ticks_diff(self.curval, self.ints) >= 0):
                        self.switchled(self.indi)
                    else:
                        self.indi = not self.indi
                        self.ints = 0
                self.ints += 1      
                
            else:
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):
            
        self.state = newState
    
    def switchled(self, val):
        if val:
            self.btmod.on()
        else:
            self.btmod.off()
            


# Equivalent Main File
    
mybt = BTconn(3,9600,8,None,1)

task1 = BTreader(0.1, 5, mybt)


for N in range(200000000):
    task1.run()


