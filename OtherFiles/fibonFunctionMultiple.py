## @file encoder.py
#  Documentation for / use of fibonFunctionMultiple.py
#
#  Detailed doc for fibonFunctionMultiple.py 
#
#  @author E. Espinoza-Wade
#
#  @copyright License Info
#
#  @date 09/20/2020
#
#  @package fibonFunctionMultiple
#
#  Brief doc for the fibonacci programs

def recurs(n):
    if n<=0:
        print("Incorrect input")
    # First Fibonacci number is 0
    elif n==1:
        return 0
    # Second Fibonacci number is 1
    elif n==2:
        return 1
    else:
        return recurs(n-1)+recurs(n-2)
 
FibArray = [0,1]
 
def dynprog(n):
    if n<=0:
        print("Incorrect input")
    elif n<=len(FibArray):
        return FibArray[n-1]
    else:
        temp_fib = dynprog(n-1)+dynprog(n-2)
        FibArray.append(temp_fib)
        return temp_fib
 
# Driver Program
 
print(dynprog(9))

