# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 00:00:27 2020

@author: wadee
"""

print ("Always executed")
def testchunk():
    if __name__ == "__main__": 
        print ("Executed when invoked directly")
    else: 
        print ("Executed when imported")