## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
# 
#  @section sec_intro Introduction
#  The following represents my portfolio. As scripts are added, additional 
#  pages and documentaion will be created to provide details on source code
#  capabilities. The links to the relevant source code will also be included
#  along with documentation for the various modules.
#
#  Included modules are:
#  * Lab0x01
#
#  @section sec_fib Fibonacci Calculation (Lab 0x01)
#  This code is meant to familiarize me with basic Python functions, and 
#  introduces concepts of algorithm development. Please see fibonFunction.fib 
#  which is part of the \ref fibonFunction package.
#
#  @author Eric Espinoza-Wade
#
#  @copyright License Info (N/A)
#
#  @date Sept. 20, 2020
#