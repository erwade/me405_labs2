## @file encoder.py
#  Documentation for / use of fibonFunction.py
#
#  Detailed doc for encoder.py 
#
#  @author E. Espinoza-Wade
#
#  @copyright License Info
#
#  @date 09/20/2020
#
#  @package fibonFunction
#  Brief doc for the encoder module
#
#  Detailed doc for the encoder module
#
#  @author Your Name
#
#  @copyright License Info
#
#  @date January 1, 1970

def fib (idx):
    print('Calculating Fibonacci number at '
          'index = {:}'.format(idx))
    
if __name__ == '__main__':
    fib(0)