## @file FSM_blink
# @brief
# @details
# @author
# @date

import time

class TaskBlink:
    '''
    '''
    
    ## Constants for states 0-4
    S0_INIT   = 0
    S1_LEDON  = 1
    S2_LEDOF  = 2

    
    def __init__(self, interval):
        self.state = self.S0_INIT
        self.interval = interval
        self.runs = 0
        
        ## Set up timing variables
        self.start_time = time.time()        
        self.interval = interval           
        self.next_time = self.start_time + self.interval        

    def run(self):
        self.curr_time = time.time()
        if (self.curr_time >= self.next_time):
            if (self.state == self.S0_INIT):
                print(str(self.runs) + ' Initialization')
                self.transitionTo(self.S1_LEDON)
    
            elif (self.state == self.S1_LEDON):
                print(str(self.runs) + ' LED on')
                self.transitionTo(self.S2_LEDOF)
            elif (self.state == self.S2_LEDOF):
                print(str(self.runs) + ' LED off')
                self.transitionTo(self.S1_LEDON)
            else:
                pass
            
            self.runs += 1
            self.next_time += self.interval
        
    def transitionTo(self, newState):
        self.state = newState

