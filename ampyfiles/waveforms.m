% This file generates plots for the 0x02b lab

clear all
close all

time = [0:0.01:100];

% subplot(3,1,1)
% plot(time,square(2*pi.*time),'LineWidth',2,'Color',[0 153 0]./255)
% axis([0 10 0 1.5])
% set(gca,'LineWidth',1,'FontSize',18,'YTick',[0 1],'YTickLabel',{'0';'V_M'})

subplot(2,1,1)
plot(time,0.5.*sin(0.2*pi.*time)+0.5,'LineWidth',3,'Color',[0 153 0]./255)
axis([0 50 0 1.5])
set(gca,'LineWidth',1,'FontSize',18,'YTick',[0 1],'YTickLabel',{'0';'V_M'})

subplot(2,1,2)
plot(time,0.5.*sawtooth(0.2*pi.*time)+0.5,'LineWidth',3,'Color',[0 153 0]./255)
axis([0 50 0 1.5])
set(gca,'LineWidth',1,'FontSize',18,'YTick',[0 1],'YTickLabel',{'0';'V_M'})
xlabel('Time [s]')
