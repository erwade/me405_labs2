## @file fibonFunction.py 
#
#  Documentation for / use of fibonFunction.py
#
#  This file calculates the Fibonacci sequence according to a user-provided
#  index (note that the Fibonacci sequency begins at index n = 0).
#
#  @package fibonFunction
#  Documentation for the fibonFunction package. 
#
#  This package contains (at least) one version of a Fibonacci sequence 
#  generator. It might be a good idea to include multiple versions to 
#  evaluate how long it takes the algorithm to run (especially for large indices),
#  as described by C. Refvem in class.
#
#  The file is available at: https://bitbucket.org/erwade/examplefiles/src/master/fibonFunction.py
#
#  @author E. Espinoza-Wade
#  
#  @date Sept. 21, 2020

import numpy as np


##  Fibonacci Sequence Generator
#
#  This function takes the user-defined idx and then determines the full 
# ibonacci sequence (note that our sequence begins with [0 1] (the 0th and 1st 
# values, respectively). 
#  It returns the full sequence, the sequence length, and the sequence value 
# corresponding to the provided index. 


def fib (idx):
## @var int idx
#
# User input for desired Fibonacci index.
    idx = int(idx)
## @var list output
#
# Seed values for Fibonacci sequence.
    output = [0,1]
    print("|--- Fibonnaci Sequence Generator for n = {:}".format(idx),"---|")
    print("               ")
    
    ind = idx;

    if ind < 0:
        print('Sequence not defined for n<2!')
    else:
        output = np.zeros(ind+1); """Create vector of length index+1"""
        
        if ind == 0:
            output[0] = 0;
        elif ind == 1:
            output[0] = 0;
            output[1] = 1;
        else:
            output[0] = 0;
            output[1] = 1;    
            cnt = 2;
            while cnt <= ind:
                output[cnt] = output[cnt-1] + output[cnt-2] 
                cnt += 1
            pass
        print("The Fibonacci index is {:}".format(ind))
        print(' ')
        print("The resulting sequence is:")    
        
        cnt = 0
        while cnt < ind+1:
            print((output[cnt]))
            cnt += 1
        

if __name__ == "__main__": 
    fib(2)
else:
    print('Cool function, bro') 
    fib(1)
    
    


