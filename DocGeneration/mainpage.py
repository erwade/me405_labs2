## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_port Portfolio Details  
#  The following represents my portfolio. See individual modules for detals.
#
#  Included modules are:
#  * Lab0x00 (N/A)
#  * Lab0x01 (\ref sec_fib)
#  * Lab0x02 (N/A)
#  * Lab0x03 (N/A)
#  * Lab0x04 (N/A)
#
#  @section sec_fib Fibonacci Calculation
#  This code is meant to familiarize me with basic Python functions, and 
#  introduces concepts of algorithm development. 
#  * Source: https://bitbucket.org/erwade/examplefiles/src/master/fibonFunction.py
#  * Documentation: \ref fibonFunction
#
#  @section sec_lab2 Lab 0x02 Documentation
#  I don't exist yet!
#  @image html ishiguro.png width=5px

#  @author Eric Espinoza-Wade
#
#  @copyright License Info (N/A)
#
#  @date Sept. 20, 2020
#