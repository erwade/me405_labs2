'''New stuff'''

from pyb import Pin, Timer, ADC, I2C
import utime
from encoder import QuadEncoder
from math import sin, cos
from BNO055 import BNO055

nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)

PWM_TIMER = Timer(3, freq=20000)

IN1 = PWM_TIMER.channel(1, pin=Pin.cpu.B4, mode=Timer.PWM, pulse_width_percent = 0)
IN2 = PWM_TIMER.channel(2, pin=Pin.cpu.B5, mode=Timer.PWM, pulse_width_percent = 0)
IN3 = PWM_TIMER.channel(3, pin=Pin.cpu.B0, mode=Timer.PWM, pulse_width_percent = 0)
IN4 = PWM_TIMER.channel(4, pin=Pin.cpu.B1, mode=Timer.PWM, pulse_width_percent = 0)

def M1_set_duty(duty):
    if duty > 0:
        IN1.pulse_width_percent(duty)
        IN2.pulse_width_percent(0)
    else:
        IN1.pulse_width_percent(0)
        IN2.pulse_width_percent(-duty)

def M2_set_duty(duty):
    if duty > 0:
        IN3.pulse_width_percent(duty)
        IN4.pulse_width_percent(0)
    else:
        IN3.pulse_width_percent(0)
        IN4.pulse_width_percent(-duty)



E1 = QuadEncoder(Pin.cpu.B6, Pin.cpu.B7, Timer(4), 1000)
E2 = QuadEncoder(Pin.cpu.C6, Pin.cpu.C7, Timer(8), 1000)

E1.use_callback(Timer(6))
E2.use_callback(Timer(7))

# E1_TIMER = Timer(4, period=0xFFFF, prescaler=0)
# E1_TIMER.channel(1, mode=Timer.ENC_AB, pin=Pin.cpu.B6)
# E1_TIMER.channel(2, mode=Timer.ENC_AB, pin=Pin.cpu.B7)

# E2_TIMER = Timer(8, period=0xFFFF, prescaler=0)
# E2_TIMER.channel(1, mode=Timer.ENC_AB, pin=Pin.cpu.C6)
# E2_TIMER.channel(2, mode=Timer.ENC_AB, pin=Pin.cpu.C7)

Kp_in = 0.5
Kd_in = 0.2
Kp_out = 10
Kd_out = 0.8

ADC_xm = ADC(Pin.cpu.A0)
PIN_xm = Pin(Pin.cpu.A0, mode=Pin.IN)

ADC_yp = ADC(Pin.cpu.A1)
PIN_yp = Pin(Pin.cpu.A1, mode=Pin.IN)

ADC_xp = ADC(Pin.cpu.A6)
PIN_xp = Pin(Pin.cpu.A6, mode=Pin.IN)

ADC_ym = ADC(Pin.cpu.A7)
PIN_ym = Pin(Pin.cpu.A7, mode=Pin.IN)


i2c = I2C(1, I2C.MASTER)
IMU = BNO055(i2c)
utime.sleep_ms(50)
IMU.enable_ndof()
utime.sleep_ms(50)

def test_imu():
    n = 0
    while n < 1000:
        n += 1
        print(IMU.get_euler())
        utime.sleep_ms(500)

def test_touch():
    n = 0
    while n < 1000:
        n += 1
    
        PIN_ym.init(mode=Pin.ANALOG)
        ADC_ym = ADC(PIN_ym)
        PIN_yp.init(mode=Pin.ANALOG)
        ADC_yp = ADC(PIN_yp)
        
        PIN_xm.init(mode=Pin.OUT_PP, value=0)
        PIN_xp.init(mode=Pin.OUT_PP, value=1)
        
        utime.sleep_us(50)
        
        x = ( ADC_ym.read() + ADC_yp.read() ) / 8090
        
        PIN_xm.init(mode=Pin.ANALOG)
        ADC_xm = ADC(PIN_xm)
        PIN_xp.init(mode=Pin.ANALOG)
        ADC_xp = ADC(PIN_xp)
        
        PIN_ym.init(mode=Pin.OUT_PP, value=0)
        PIN_yp.init(mode=Pin.OUT_PP, value=1)
        
        utime.sleep_us(50)
        
        y = ( ADC_xm.read() + ADC_xp.read() ) / 8090
        
        PIN_ym.init(mode=Pin.ANALOG)
        ADC_ym = ADC(PIN_ym)
        PIN_xp.init(mode=Pin.ANALOG)
        ADC_xp = ADC(PIN_xp)
        
        PIN_xm.init(mode=Pin.OUT_PP, value=0)
        PIN_yp.init(mode=Pin.OUT_PP, value=1)
        
        utime.sleep_us(50)
        
        z = 1 - ( ADC_ym.read() - ADC_xp.read() ) / 4095
        
        
        # utime.sleep_ms(398)
        
        if(z > 0.1):
            print('{:},{:}\n'.format(x,y))

def go():
    E1.zero()
    E2.zero()
    
    utime.sleep_ms(100)

    nSLEEP.high()
    
    utime.sleep_ms(100)
    
    x = 0
    y = 0
    
    n=0
    while n < 5000:
        n += 1
        
        PIN_ym.init(mode=Pin.ANALOG)
        ADC_ym = ADC(PIN_ym)
        PIN_yp.init(mode=Pin.ANALOG)
        ADC_yp = ADC(PIN_yp)
        
        PIN_xm.init(mode=Pin.OUT_PP, value=0)
        PIN_xp.init(mode=Pin.OUT_PP, value=1)
        
        utime.sleep_us(50)
        
        last_x = x
        x  = 5 * ( ( ADC_ym.read() + ADC_yp.read() ) / 4095 - 1 )
        xd = x - last_x
        
        
        PIN_xm.init(mode=Pin.ANALOG)
        ADC_xm = ADC(PIN_xm)
        PIN_xp.init(mode=Pin.ANALOG)
        ADC_xp = ADC(PIN_xp)
        
        PIN_ym.init(mode=Pin.OUT_PP, value=0)
        PIN_yp.init(mode=Pin.OUT_PP, value=1)
        
        utime.sleep_us(50)
        
        last_y = y
        y  =-5 * ( ( ADC_xm.read() + ADC_xp.read() ) / 4095 - 1 )
        yd = y - last_y
        
        PIN_ym.init(mode=Pin.ANALOG)
        ADC_ym = ADC(PIN_ym)
        PIN_xp.init(mode=Pin.ANALOG)
        ADC_xp = ADC(PIN_xp)
        
        PIN_xm.init(mode=Pin.OUT_PP, value=0)
        PIN_yp.init(mode=Pin.OUT_PP, value=1)
        
        utime.sleep_us(50)
        
        z = 1 - ( ADC_ym.read() - ADC_xp.read() ) / 4095
        
        th1  = E1.get_position()
        thd1 = E1.get_delta()
        
        th2  = E2.get_position()
        thd2 = E2.get_delta()
        
        # r1 = 150*sin(2*3.14*n/100)
        # r2 = 150*sin(2*3.14*n/100) 
        eul = IMU.get_euler()
        gyr = IMU.get_gyro()
        
        phi1 = -eul[2]
        phi2 = eul[1]
        
        phi1d = gyr[0]
        phi2d = gyr[1]
        
        if(z > 0.1):
            r1 = -Kp_out*x - Kd_out*xd - Kp_out*phi1
            r2 = -Kp_out*y - Kd_out*yd - Kp_out*phi2
        else:
            r1 = -Kp_out*phi1
            r2 = -Kp_out*phi2
        
        M1_set_duty(Kp_in*(r1 - thd1))
        M2_set_duty(Kp_in*(r2 - thd2))
        
        utime.sleep_ms(10)
        
    nSLEEP.low()